package mysql_to_hl7.mysql_to_hl7;

import java.util.Date;

public class Users {

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private int user_id;
	private String pseudonym;
	private String name;
	private Date start_date;
	private int study_day;
	private String status;
	private String login_id;
	private String login_pin;
	private String character;
	private String language;
	private String email;
	
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getPseudonym() {
		return pseudonym;
	}

	public void setPseudonym(String pseudonym) {
		this.pseudonym = pseudonym;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public int getStudy_day() {
		return study_day;
	}

	public void setStudy_day(int study_day) {
		this.study_day = study_day;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getLogin_pin() {
		return login_pin;
	}

	public void setLogin_pin(String login_pin) {
		this.login_pin = login_pin;
	}

	public String getCharacter() {
		return character;
	}

	public void setCharacter(String character) {
		this.character = character;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
