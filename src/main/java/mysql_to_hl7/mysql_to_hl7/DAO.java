package mysql_to_hl7.mysql_to_hl7;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class DAO {
	Connection connection = null;

	DAO() {
		try {
			Class.forName("com.mysql.jdbc.Driver");

			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/familyhistory", "root", "");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();

		}
	}

	public Connection getConnection() {

		return null;

	}

	public ArrayList getUsers() {
		Statement statement;
		String query = "SELECT * FROM users";
		ResultSet resultSet = null;
		ArrayList<Users> users = new ArrayList();
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				Users user = new Users();
				user.setUser_id(resultSet.getInt(1));
				user.setPseudonym(resultSet.getString(2));
				user.setName(resultSet.getString(3));
				user.setStart_date(resultSet.getDate(4));
				user.setStudy_day(resultSet.getInt(5));
				user.setStatus(resultSet.getString(6));
				user.setLogin_id(resultSet.getString(7));
				user.setLogin_pin(resultSet.getString(8));
				user.setCharacter(resultSet.getString(9));
				user.setLanguage(resultSet.getString(10));
				user.setEmail(resultSet.getString(11));
				users.add(user);
			}
			return users;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public HashMap getProperties(String query) {
		Statement statement;
		ResultSet resultSet = null;
		
		
		
		HashMap<String, String> properties = new HashMap();

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				properties.put(resultSet.getString(2), resultSet.getString(3));
			}
			return properties;
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}