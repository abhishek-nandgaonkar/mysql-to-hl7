package mysql_to_hl7.mysql_to_hl7;

import java.util.HashMap;

public class UsersAndProperties {

	

	private Users users;
	private HashMap<String, String> usersProperties = new HashMap();

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public HashMap<String, String> getUsersProperties() {
		return usersProperties;
	}

	public void setUsersProperties(HashMap<String, String> usersAndProperties) {
		this.usersProperties = usersAndProperties;
	}

}
