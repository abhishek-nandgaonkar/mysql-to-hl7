package mysql_to_hl7.mysql_to_hl7;

import java.util.ArrayList;
import java.util.HashMap;

public class App {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		ArrayList<Users> users = new ArrayList<Users>();
		ArrayList<UsersAndProperties> usersAndPropertiesList = new ArrayList<UsersAndProperties>();
		
		DAO dao = new DAO();
		users = dao.getUsers();

		for (Users user : users) {
			UsersAndProperties usersAndProperties = new UsersAndProperties();
			String query = "select * from familyhistory.properties where user_id=" + user.getUser_id();
			HashMap<String, String> usersProperties = new HashMap<String, String>();
			usersProperties = dao.getProperties(query);
			usersAndProperties.setUsers(user);
			usersAndProperties.setUsersProperties(usersProperties);
			usersAndPropertiesList.add(usersAndProperties);
		}

		System.out.println("Validating stored data");
		for (UsersAndProperties usersAndProperties1 : usersAndPropertiesList) {
			for (String name : usersAndProperties1.getUsersProperties().keySet()) {

				String key = name.toString();
				String value = usersAndProperties1.getUsersProperties().get(name).toString();
				System.out.println(key + " " + value);

			}
			
		}
		
		
	}
}
